var mongoose = require('mongoose');

var PinSchema = new mongoose.Schema({
	id: {
		type:String,
		require:true
	},

	latitude: {
		type: String,
		require: true
	},

	longitude: {
		type: String,
		require: true
	}
})

module.exports = mongoose.model('Pin', PinSchema)
