/*eslint-env node*/

//------------------------------------------------------------------------------
// node.js starter application for Bluemix
//------------------------------------------------------------------------------

// This application uses express as its web server
// for more info, see: http://expressjs.com
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

// cfenv provides access to your Cloud Foundry environment
// for more info, see: https://www.npmjs.com/package/cfenv
var cfenv = require('cfenv');

// create a new express server
var app = express();

//connect to mongoose
mongoose.connect('mongodb://IbmCloud_n3upvkag_7rmgk2ga_hvuj0neg:2sv9otBm2GPxEDEj-BvfbU6DTQaOb8wi@ds041053.mongolab.com:41053/IbmCloud_n3upvkag_7rmgk2ga');

// serve the files out of ./public as our main files
//app.use(express.static(__dirname + '/public'));

// POST Handler
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

//routers
var pinRouter = require('./routes/PinRouter');
app.use("/api", pinRouter)

// get the app environment from Cloud Foundry
var appEnv = cfenv.getAppEnv();

// start server on the specified port and binding host
app.listen(appEnv.port, function() {

	// print a message when the server starts listening
  console.log("server starting on " + appEnv.url);
  //need to route requests somehow
});

