var app = require('express');
var router = app.Router();
var Pin = require('../models/Pin');

/*have to call use before route can be called
  all requests will hit this first
*/

router.use('/pinrouter', function(req, res, next) {
 	next();
})

router.route('/pinrouter')
	.get(function(req, res) {
		//console.log('pins')
		res.json({message: 'pin'});
	})
	.post(function(req, res) {
		//insert data into database
		/*before inserting, always check if it exists in db
		already with the specific id? you assume it's not in database?
		*/
		console.log(req.method);
		console.log(req.body);

		//inserts pin if it's not already in db
		var trimmedId = req.body.id.trim()
		Pin.findOne({id: trimmedId}, function(err, pinResult) {
			if(err) {
				console.log("err");
			} else {
				if(pinResult == null) { //pin doesn't exist
					//insert pin
					var pin = new Pin();
					pin.id = trimmedId;
					pin.latitude = req.body.latitude;
					pin.longitude = req.body.longitude;
		
					pin.save(function(err) {
						if(err) {
							res.send(err); 
						} else {
							res.json({message: "pin added"});
						}
					})
				} else { // pin exists so do nothing
					res.json({message: "pin already exists"})
				}
			}
		})
		/*
		Pin.findOneAndUpdate(
			{id: id, latitude: lat, longitude: longi}, //query
			{id: id, latitude: lat, longitude: longi}
			)
	*/
	})
	.put(function(req, res) {

	})


module.exports = router